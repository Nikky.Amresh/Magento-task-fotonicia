# Adding Custom tab in magento 2 product page

1. Install magento to our website
2. Now login to the admin panel
3. Then we'll go to stores=>products
4. Here we'll add ' new attribute' for 'tab details'
5. Thenafter we need to set the new attribute to 'default' or custom attribute set
6. We will setup xml layout for this new tab as

```
     <?xml version="1.0"?>
     <page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
     <body>
     <referenceBlock name="product.info.details">
      <block class="Magento\Catalog\Block\Product\View" name="stock.tab" template="test_module::stock.phtml" group="detailed_info" >
     <arguments>
      <argument translate="true" name="title" xsi:type="string">Stock</argument>
      </arguments>
      </block>
     </referenceBlock>
      </body>
     </page>
```

7. We have to save this file to the location of installed  magento as app/code/test/module/view/frontend/layout/catalog_product_view.xml 
8. After it we'll setup our template file as

```
     <?php
     $product = $block->getProduct();
    ?>
    <h1><?php echo $product->getData('stock'); ?></h1>
       
```

7. We have to save this file to the location of installed  magento as app/code/test/module/view/frontend/templates/stock.phtml


Now when we'll add any new product with admin panel we'll have an extra option 'stock'

After adding product we'll reindex it

Finally we'll get this result


![screenshot](https://github.com/NikkyAmresh/Magento-task-fotonicia/raw/master/screenshots/pic.png)
 ![screenshot](https://github.com/NikkyAmresh/Magento-task-fotonicia/raw/master/screenshots/pic1.png)
